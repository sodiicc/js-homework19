let data = {
  "Рыбы": {
    "Форель": {},
    "Щука": {}
  },
  "frog": 55,
  "apple": 22,
  "Деревья": {
    "Хвойные": {
      "Лиственница": {},
      "Ель": {}
    },
    "Цветковые": {
      "Берёза": {},
      "Тополь": {}
    }

  }
};

function cloneObj(obj) {
  let newObj = {};
  for (let key in obj) {
    if (typeof obj[key] == 'object') {
      newObj[key] = cloneObj(obj[key])
    }else{
      newObj[key] = obj[key];
    }
  }
  return newObj;
};

// let newObj = cloneObj(data)
// console.log('newObj', newObj)

// newObj.name = 'fsfs';
// data["Рыбы"]['Форель'] = 'safas'
// console.log('newObj', newObj)
// console.log('data', data)